package de.danielthiem.photoprismhistory

import android.content.Context
import android.graphics.drawable.shapes.Shape
import android.os.Bundle
import android.util.Log
import android.view.MotionEvent
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.animation.core.animateFloatAsState
import androidx.compose.animation.core.animateOffsetAsState
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.gestures.*
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.geometry.RoundRect
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.asImageBitmap
import androidx.compose.ui.graphics.graphicsLayer
import androidx.compose.ui.input.pointer.PointerEventPass
import androidx.compose.ui.input.pointer.PointerInputChange
import androidx.compose.ui.input.pointer.pointerInput
import androidx.compose.ui.input.pointer.pointerInteropFilter
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.input.PasswordVisualTransformation
import androidx.compose.ui.unit.dp
import androidx.compose.ui.zIndex
import androidx.lifecycle.LifecycleCoroutineScope
import androidx.lifecycle.lifecycleScope
import cafe.adriel.voyager.androidx.AndroidScreen
import cafe.adriel.voyager.core.lifecycle.LifecycleEffect
import cafe.adriel.voyager.hilt.getScreenModel
import cafe.adriel.voyager.navigator.LocalNavigator
import cafe.adriel.voyager.navigator.Navigator
import cafe.adriel.voyager.navigator.currentOrThrow
import dagger.hilt.android.AndroidEntryPoint
import de.danielthiem.photoprismhistory.data.*
import de.danielthiem.photoprismhistory.ui.theme.PhotoprismHistoryTheme
import jp.wasabeef.blurry.Blurry
import kotlinx.coroutines.awaitCancellation
import java.time.format.DateTimeFormatter

@ExperimentalComposeUiApi
@AndroidEntryPoint
class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContent {
            Navigator(LoginScreen())
        }
    }
}


@ExperimentalComposeUiApi
class LoginScreen() : AndroidScreen() {
    @Composable
    override fun Content() {
        PhotoprismHistoryTheme {
            val screenModel = getScreenModel<LoginScreenModel>()
            val credentials by screenModel.data.collectAsState()
            val isLoading by screenModel.isLoading.collectAsState()
            val navigator = LocalNavigator.currentOrThrow
            LifecycleEffect(
                onStarted = {
                    screenModel.checkForCurrentCachedImages { hasCachedImages ->
                        if (hasCachedImages) {
                            navigator push OverviewScreen(loadFromCache = true)
                        } else {
                            screenModel.tryLoginFromStoredCredentials { result ->
                                println("umm?")
                                if (result.successful) {
                                    println("Got success")
                                    navigator push OverviewScreen()
                                } else {
                                    println("Got no success")
                                    //TODO
                                }
                            }
                        }
                    }

                })
            Surface(color = MaterialTheme.colors.background) {
                Box(
                    modifier = Modifier.fillMaxSize(),
                    contentAlignment = Alignment.Center
                ) {
                    when (isLoading) {
                        true -> CircularProgressIndicator()
                        false -> LoginForm(credentials, screenModel, navigator)
                    }


                }
            }
        }
    }

    @Composable
    private fun LoginForm(
        credentials: PhotoPrismCredentials,
        screenModel: LoginScreenModel,
        navigator: Navigator
    ) {
        Column(
            verticalArrangement = Arrangement.Center,
        ) {
            Text(text = stringResource(R.string.login_title), style = MaterialTheme.typography.h5)
            OutlinedTextField(
                value = credentials.hostUrl,
                onValueChange = { screenModel.updateHost(it) },
                label = { Text(stringResource(id = R.string.host_name)) },
                placeholder = { Text("https://...") },
                keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Email)
            )
            Spacer(modifier = Modifier.size(4.dp))


            OutlinedTextField(
                value = credentials.username,
                onValueChange = { screenModel.updateUsername(it) },
                label = { Text(stringResource(id = R.string.username)) }
            )

            OutlinedTextField(
                value = credentials.password,
                onValueChange = { screenModel.updatePassword(it) },
                label = { Text(stringResource(id = R.string.password)) },
                keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Password),
                visualTransformation = PasswordVisualTransformation()
            )
            Spacer(modifier = Modifier.size(4.dp))
            val onResult = { res: PhotoprismDataSource.LoginResultData ->
                if (res.successful) {
                    screenModel.installWorkRequests(9)
                    navigator push OverviewScreen()
                } else {
                    //TODO
                }
            }
            Button(
                modifier = Modifier.padding(4.dp),
                onClick = {
                    screenModel.tryLogin(onResult = onResult)
                }) {
                Text(stringResource(id = R.string.login_button))
            }
        }
    }
}



val prettyDateTimeFormatter = DateTimeFormatter.ofPattern("dd.MM.YYYY")
@ExperimentalComposeUiApi
class OverviewScreen(val loadFromCache:Boolean = false) : AndroidScreen() {

    private val TAG = "ImageScreen"

    @ExperimentalMaterialApi
    @Composable
    override fun Content() {
        val imagesModel = getScreenModel<ImagesScreenModel>()
        LifecycleEffect(onStarted = {
            if (loadFromCache) {
                imagesModel.loadImagesFromCache()
            } else {
                imagesModel.requestTodaysImages()
            }
        })
        PhotoprismHistoryTheme {
            Surface(color = MaterialTheme.colors.background) {
                val images by imagesModel.images.collectAsState()
                val currentImage by imagesModel.currentImage.collectAsState()
                val currentIndex by imagesModel.currentIndex.collectAsState()

                if (images.isNotEmpty() && currentImage != null) {
                    BottomSheetScaffold(
                        sheetContent = {

                            Button(onClick =  {
                                imagesModel.testWorker()
                            }) {
                                Text("Test Worker")
                            }
                            ImageDescription(currentImage)
                        },

                        sheetPeekHeight = 64.dp,
                        sheetElevation = 16.dp,
                        sheetGesturesEnabled = true
                    ) {


                        Box(
                            modifier = Modifier
                                .background(Color.DarkGray)
                                .fillMaxSize()
                        ) {
                            FullscreenImage(currentImage, imagesModel)

                            TopImageIndicators(images, currentIndex)
                        }
                    }


                } else {
                    Box(
                        modifier = Modifier.fillMaxSize(),
                        contentAlignment = Alignment.Center
                    ) {
                        CircularProgressIndicator()


                    }
                }

            }
        }
    }


    @Composable
    private fun ImageDescription(currentImage: ImageUiState?) {
        Column(
            verticalArrangement = Arrangement.Top,
            modifier = Modifier
                .fillMaxHeight()
                .padding(4.dp)
        ) {
            Text(currentImage!!.title, style = MaterialTheme.typography.h5)
            Text(
                currentImage!!.dateTaken.format(prettyDateTimeFormatter),
                style = MaterialTheme.typography.body2
            )

        }
    }

    @ExperimentalComposeUiApi
    @Composable
    private fun FullscreenImage(currentImage: ImageUiState?, imagesModel: ImagesScreenModel) {
        var scale by remember { mutableStateOf(1f) }
        var offset by remember { mutableStateOf(Offset.Zero) }


        var returnNow by remember { mutableStateOf(false) }
        val returnAnimation by animateFloatAsState(targetValue = if (returnNow) 1f else scale)
        val returnOffsetAnimation by animateOffsetAsState(targetValue = if (returnNow) Offset.Zero else offset)

        /*
        var state = rememberTransformableState { zoomChange, panChange, rotationChange ->
            scale *= zoomChange
            offset += panChange
        }*/



        BoxWithConstraints(
            Modifier.fillMaxSize()
        ) {
            val maxWidthPx = with(LocalDensity.current) {
                maxWidth.toPx()
            }
            Image(
                bitmap = currentImage!!.bitmap.asImageBitmap(),
                contentScale = ContentScale.FillWidth,
                contentDescription = "Fill",
                modifier = Modifier
                    .fillMaxSize()
                    .padding(4.dp)
                    .background(Color.Transparent)
                    .graphicsLayer {
                        scaleX = returnAnimation
                        scaleY = returnAnimation
                        translationX = returnOffsetAnimation.x
                        translationY = returnOffsetAnimation.y
                    }
                    //.transformable(state = state)
                    .pointerInput(Unit) {
                        detectTapGestures(
                            onTap = {
                                if (it.x > maxWidthPx / 2) {
                                    imagesModel.nextImage()
                                } else {
                                    imagesModel.previousImage()
                                }
                            })

                        //detectTransformGestures()

                    }
                    .pointerInputDetectTransformGestures(
                        isTransformInProgressChanged = { newValue ->
                            if (!newValue) {
                                returnNow = true
                                scale = 1f
                                offset = Offset.Zero
                            }
                        },
                        onGesture = { gOffset, gPan, gestureZoom, gestureRotate ->
                            returnNow = false
                            scale *= gestureZoom
                            offset += gPan
                        }
                    )


            )
        }
    }

    fun Modifier.pointerInputDetectTransformGestures(
        panZoomLock: Boolean = false,
        isTransformInProgressChanged: (Boolean) -> Unit,
        onGesture: (centroid: Offset, pan: Offset, zoom: Float, rotation: Float) -> Unit
    ): Modifier {
        return pointerInput(Unit) {
            detectTransformGestures(
                panZoomLock = panZoomLock,
                onGesture = { offset, pan, gestureZoom, gestureRotate ->
                    isTransformInProgressChanged(true)
                    onGesture(offset, pan, gestureZoom, gestureRotate)
                }
            )
        }.pointerInput(Unit) {
            forEachGesture {
                awaitPointerEventScope {
                    awaitFirstDown(requireUnconsumed = false)
                    do {
                        val event = awaitPointerEvent()
                        val canceled = event.changes.any { it.consumed.positionChange }
                    } while (!canceled && event.changes.any { it.pressed })
                    isTransformInProgressChanged(false)
                }
            }
        }
    }

    @Composable
    private fun TopImageIndicators(
        images: List<ImageUiState>,
        currentIndex: Int
    ) {
        Row(
            modifier = Modifier
                .fillMaxWidth()
                .zIndex(20f)
                .padding(8.dp)
        ) {
            for (index in images.indices) {
                val color = when {
                    index == currentIndex -> Color(1f, 1f, 1f, 0.9f)
                    else -> Color(1f, 1f, 1f, 0.5f)
                }
                Box(
                    modifier = Modifier
                        .fillMaxWidth()
                        .height(4.dp)
                        .background(color, RoundedCornerShape(2.dp))
                        .weight(1f)
                        .padding(1.dp)
                )
            }
        }
    }


}

/*
@Preview(showBackground = true)
@Composable
fun DefaultPreview() {
    PhotoprismHistoryTheme {
        Greeting("Android")
    }
}*/