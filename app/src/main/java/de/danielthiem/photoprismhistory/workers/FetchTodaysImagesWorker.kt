package de.danielthiem.photoprismhistory.workers

import android.content.Context
import android.graphics.Bitmap
import androidx.core.graphics.BitmapCompat
import androidx.hilt.work.HiltWorker
import androidx.work.CoroutineWorker
import androidx.work.Worker
import androidx.work.WorkerParameters
import com.bumptech.glide.disklrucache.DiskLruCache
import dagger.assisted.Assisted
import dagger.assisted.AssistedInject
import de.danielthiem.photoprismhistory.data.CachedImagesRepository
import de.danielthiem.photoprismhistory.data.CredentialRepository
import de.danielthiem.photoprismhistory.data.PhotoprismDataSource
import de.danielthiem.photoprismhistory.db.CachedImage
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.withContext
import java.io.ByteArrayOutputStream
import java.io.File
import java.time.LocalDateTime

@HiltWorker
class FetchTodaysImagesWorker @AssistedInject constructor(
    @Assisted appcontext: Context,
    @Assisted workerParams: WorkerParameters,
    val photoprismDataSource: PhotoprismDataSource,
    val credentialRepository: CredentialRepository,
    val cachedImagesRepository: CachedImagesRepository
) : CoroutineWorker(appcontext, workerParams) {




    override suspend fun doWork(): Result {
        val credentials = credentialRepository.getCredentials()
        if (credentials != null) {
            if (!photoprismDataSource.trySavedSession(credentials.getCheckedHostUrl())) {
                //Login via saved session failed, falling back to login with credentials
                val loginResult = photoprismDataSource.login(credentials, false)
                if (!loginResult.successful) {
                    return Result.failure()
                }
            }
            return withContext(Dispatchers.IO) {
                val imageFlow = photoprismDataSource.getHistoryOfToday()
                val now = LocalDateTime.now()
                if (imageFlow != null) {
                    val out = imageFlow.map { image ->
                        async(Dispatchers.IO) {
                            cachedImagesRepository.storeFromImageUiState(
                                image,
                                now
                            )
                        }
                    }.collect {
                        it.await()
                    }
                    return@withContext Result.success()
                } else {
                    return@withContext Result.failure()
                }
            }

        }
        return Result.failure()
    }

}