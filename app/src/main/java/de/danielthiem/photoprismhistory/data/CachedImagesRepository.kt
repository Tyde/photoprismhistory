package de.danielthiem.photoprismhistory.data

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.util.Log
import dagger.hilt.android.qualifiers.ApplicationContext
import de.danielthiem.photoprismhistory.db.CachedImage
import de.danielthiem.photoprismhistory.db.CachedImageDao
import de.danielthiem.photoprismhistory.db.Converters
import kotlinx.coroutines.*
import java.io.ByteArrayOutputStream
import java.io.File
import java.time.LocalDate
import java.time.LocalDateTime
import javax.inject.Inject


class CachedImagesRepository @Inject constructor(
    @ApplicationContext val appContext: Context,
    val cachedImageDao: CachedImageDao,
    val converters: Converters
) {
    val TAG = "CachedImagesRepository"

    private fun getImageInfoForToday(): List<CachedImage> {
        val today = LocalDate.now().atStartOfDay()
        val tomorrow = today.plusDays(1)
        val beginTs = converters.dateToTimestamp(today)
        val endTs = converters.dateToTimestamp(tomorrow)
        if (beginTs != null && endTs != null) {
            return cachedImageDao.getAllBetweenTimestamps(
                beginTs,
                endTs
            )
        } else {
            return listOf()
        }
    }

    suspend fun getImagesForToday(): List<ImageUiState> = coroutineScope {
        val imageInfo = getImageInfoForToday()
        Log.d(TAG,imageInfo.toString())
        println("Test")
        imageInfo.map { cachedImage ->
            async {
                Log.d(TAG, "Opening: "+cachedImage.hash + ".bitmap in ${appContext.cacheDir}")
                File(appContext.cacheDir, cachedImage.fileName)
                    .inputStream()
                    .use { fis ->
                        val bitmap = BitmapFactory.decodeStream(fis)
                        if (bitmap != null) {
                            ImageUiState(
                                bitmap = bitmap,
                                title = cachedImage.title,
                                dateTaken = cachedImage.takenAt,
                                hash = cachedImage.hash,
                                photoID = cachedImage.photoPrismId,
                                photoUID = cachedImage.photoPrismUid
                            )
                        } else {
                            null
                        }
                    }
            }
        }.awaitAll().filterNotNull()
    }

    fun hasImangeForToday(): Boolean {
        val infos = getImageInfoForToday()
        return infos.isNotEmpty()
    }

    fun storeCachedImage(cachedImage: CachedImage) {
        cachedImageDao.insertCachedImage(cachedImage)
    }

    fun storeFromImageUiState(
        image: ImageUiState,
        timeDownloaded: LocalDateTime
    ) {
        val cachedImage = CachedImage(
            dbID = 0,
            hash = image.hash,
            timeDownloaded = timeDownloaded,
            photoPrismId = image.photoID,
            photoPrismUid = image.photoUID,
            takenAt = image.dateTaken,
            title = image.title,
            fileName = null
        )
        val insertID = cachedImageDao.insertCachedImage(cachedImage)
        val file = File.createTempFile(
            image.hash,
            ".bitmap",
            appContext.cacheDir
        )
        file.outputStream().use { stream ->
            val bos = ByteArrayOutputStream()
            image.bitmap.compress(Bitmap.CompressFormat.PNG, 0, bos)
            stream.write(bos.toByteArray())
            stream.flush()
        }
        val updatedCachedImage = cachedImageDao.getById(insertID).copy(fileName = file.name)
        cachedImageDao.updateCachedImage(updatedCachedImage)

    }
}