package de.danielthiem.photoprismhistory.data

import android.content.Context
import android.content.SharedPreferences
import androidx.security.crypto.EncryptedSharedPreferences
import androidx.security.crypto.MasterKey
import dagger.hilt.android.HiltAndroidApp
import dagger.hilt.android.qualifiers.ApplicationContext
import de.danielthiem.photoprismhistory.PhotoprismHistoryApplication
import java.io.File
import java.lang.RuntimeException
import java.security.KeyStore
import java.time.Duration
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.time.format.DateTimeFormatter.BASIC_ISO_DATE
import java.time.format.DateTimeFormatter.ISO_OFFSET_DATE_TIME
import javax.inject.Inject
import javax.inject.Singleton


data class PhotoPrismCredentials(val hostUrl: String, val username: String, val password: String) {
    fun getCheckedHostUrl(): String {
        if (!hostUrl.endsWith("/")) {
            return hostUrl + "/"
        } else {
            return hostUrl
        }
    }
}

interface CredentialRepository {
    fun saveCredentials(creds: PhotoPrismCredentials)
    fun getCredentials(): PhotoPrismCredentials?
    fun saveCurrentSession(savedSession: SavedSession)
    fun getCurrentSession(): SavedSession?
}

data class SavedSession(val sessionUID: String, val lastUsed: LocalDateTime)

@Singleton
class CredentialRepositoryKeystoreImpl @Inject constructor(
    @ApplicationContext private val appContext: Context
) : CredentialRepository {

    val PREFERENCES_NAME = "credentials"

    val HOST_LABEL = "host"
    val USERNAME_LABEL = "username"
    val PASSWORD_LABEL = "password"

    val SESSION_UID = "session_uid"
    val SESSION_LAST_USED = "session_last_used"

    override fun saveCredentials(creds: PhotoPrismCredentials) {
        val sharedPreferences = getSharedPreferences()
        with(sharedPreferences.edit()) {
            putString(HOST_LABEL, creds.hostUrl)
            putString(USERNAME_LABEL, creds.username)
            putString(PASSWORD_LABEL, creds.password)
            apply()
        }
    }

    private fun getSharedPreferences(retryOnFail: Boolean = true): SharedPreferences {
        try {
            val masterKey =
                MasterKey.Builder(appContext, PhotoprismHistoryApplication.masterKeyAlias)
                    .setKeyScheme(MasterKey.KeyScheme.AES256_GCM)
                    .build()
            return EncryptedSharedPreferences.create(
                appContext,
                PREFERENCES_NAME,
                masterKey,
                EncryptedSharedPreferences.PrefKeyEncryptionScheme.AES256_SIV,
                EncryptedSharedPreferences.PrefValueEncryptionScheme.AES256_GCM
            )
        } catch (e: RuntimeException) {
            if (retryOnFail) {
                println("In retry")
                // delete the master key
                val keyStore = KeyStore.getInstance("AndroidKeyStore");
                keyStore.load(null)
                keyStore.deleteEntry(MasterKey.DEFAULT_MASTER_KEY_ALIAS)

                // delete the encrypted prefs
                appContext.getSharedPreferences(PREFERENCES_NAME, Context.MODE_PRIVATE).edit()
                    .clear()
                    .apply()
                return getSharedPreferences(false)
            } else {
                throw e
            }
        }
    }



    override fun getCredentials(): PhotoPrismCredentials? {
        val sharedPreferences = getSharedPreferences()

        val hostName = sharedPreferences.getString(HOST_LABEL, "")
        val username = sharedPreferences.getString(USERNAME_LABEL, "")
        val password = sharedPreferences.getString(PASSWORD_LABEL, "")
        if (
            hostName != null && hostName != "" &&
            username != null && username != "" &&
            password != null && password != ""
        ) {
            return PhotoPrismCredentials(hostName, username, password)
        } else {
            return null
        }

    }

    override fun saveCurrentSession(savedSession: SavedSession) {
        val sharedPreferences = getSharedPreferences()
        with(sharedPreferences.edit()) {
            putString(SESSION_UID, savedSession.sessionUID)
            putString(
                SESSION_LAST_USED,
                DateTimeFormatter.ISO_LOCAL_DATE_TIME.format(savedSession.lastUsed)
            )
            apply()
        }
    }

    private fun retrieveCurrentSession(): SavedSession? {
        val sharedPreferences = getSharedPreferences()
        val uid = sharedPreferences.getString(SESSION_UID, null)
        val lastUsed = sharedPreferences.getString(SESSION_LAST_USED, null)?.let { timeString ->
            LocalDateTime.parse(timeString, DateTimeFormatter.ISO_LOCAL_DATE_TIME)
        }
        if (uid != null && lastUsed != null) {
            return SavedSession(uid, lastUsed)
        }
        return null
    }

    override fun getCurrentSession(): SavedSession? {
        val session = retrieveCurrentSession()
        if (session != null) {
            val duration = Duration.between(LocalDateTime.now(), session.lastUsed)
            if (duration < Duration.ofDays(2)) {
                return session
            }
        }
        return null
    }

}