package de.danielthiem.photoprismhistory.data

import android.content.Context
import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import android.util.Log
import androidx.compose.ui.res.stringResource
import com.bumptech.glide.Glide
import com.bumptech.glide.request.target.CustomTarget
import com.bumptech.glide.request.transition.Transition
import com.google.gson.annotations.SerializedName
import dagger.hilt.android.qualifiers.ApplicationContext
import de.danielthiem.photoprismhistory.R
import io.ktor.client.*
import io.ktor.client.call.*
import io.ktor.client.features.*
import io.ktor.client.request.*
import io.ktor.client.request.forms.*
import io.ktor.client.statement.*
import io.ktor.http.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.launch
import java.lang.Exception
import java.net.ConnectException
import java.time.LocalDateTime
import javax.inject.Inject
import javax.inject.Singleton


@Singleton
class PhotoprismDataSource @Inject constructor(
    private val ktorClient: HttpClient,
    @ApplicationContext private val appContext: Context,
    private val credentialRepository: CredentialRepository
) {

    private val TAG = "PhotoprismDataSource"


    private var sesssionID: String? = null
    private var loggedIn: Boolean = false
    private var usedCredentials: PhotoPrismCredentials? = null

    init {

    }

    data class LoginRequestData(val username: String, val password: String)
    data class LoginResultData(val successful: Boolean, val message: String? = null)

    suspend fun login(
        credentials: PhotoPrismCredentials,
        storeCredentialsOnSuccess: Boolean = true
    ): LoginResultData {
        try {

            val url = credentials.getCheckedHostUrl() + "api/v1/session"
            val response: HttpResponse = ktorClient.post(url) {
                timeout {
                    connectTimeoutMillis = 15000
                }
                contentType(ContentType.Application.Json)
                body = LoginRequestData(credentials.username, credentials.password)
            }
            sesssionID = response.headers["X-Session-Id"]
            //val answer = response.receive<SessionAnswer>()
            loggedIn = true
            usedCredentials = credentials
            if(storeCredentialsOnSuccess) {
                credentialRepository.saveCurrentSession(
                    SavedSession(
                        sesssionID ?: "",
                        LocalDateTime.now()
                    )
                )
            }
            return LoginResultData(true)
        } catch (e: ClientRequestException) {
            val msg = e.response.receive<APIErrorMessage>()
            Log.v(TAG, "Login failed, msg is $msg")
            return LoginResultData(
                false,
                appContext.resources.getString(R.string.error_login_request_exception)
            )
        } catch (e: ConnectException) {
            Log.v(TAG, "Login failed, connection timed out")
            return LoginResultData(
                false,
                appContext.resources.getString(R.string.error_login_connection_timeout)
            )
        }
    }

    suspend fun trySavedSession(hostUrl:String): Boolean {
        Log.v(TAG,"trying saved session")
        val session =credentialRepository.getCurrentSession()
        if (session!= null) {
            Log.v(TAG,"Retrieved saved Session $session")
            try {
                val url = hostUrl + "api/v1/photos/none"
                val response: HttpResponse = ktorClient.submitForm(url, encodeInQuery = true) {
                    header("X-Session-ID", session.sessionUID)
                }
                Log.v(TAG,"Got unexcepted status ${response.status}")

            } catch (e: ClientRequestException) {
                val status = e.response.status
                Log.v(TAG,"Got Status $status")
                if(status == HttpStatusCode.NotFound) {
                    sesssionID = session.sessionUID
                    usedCredentials = credentialRepository.getCredentials()
                    loggedIn = true
                    return true
                }

            } catch (e:Exception) {
                Log.v(TAG,"Anything else? $e")
            }
        }
        return false
    }

    /**
     * Will download the relevant images for today.
     * Notice: The session has to be established already
     */
    suspend fun getHistoryOfToday(): Flow<ImageUiState>? {

        if (loggedIn && usedCredentials != null) {
            val url = usedCredentials!!.getCheckedHostUrl() + "api/v1/photos"
            val now = LocalDateTime.now()
            val response: HttpResponse = ktorClient.submitForm(
                url,
                Parameters.build {
                    append("month", "${now.monthValue}")
                    append("q", "day:${now.dayOfMonth}")
                    append("count", "60")
                },
                encodeInQuery = true
            ) {
                header("X-Session-ID", sesssionID)
            }
            credentialRepository.saveCurrentSession(SavedSession(sesssionID?:"", LocalDateTime.now()))
            val previewToken = response.headers["X-Preview-Token"] ?: ""
            val imageList= response.receive<List<PhotoResponseDAO>>().filter { photo ->
                return@filter photo.photoType == "image"
            }.distinctBy { it.photoUID }


            Log.v(TAG, "Got Response: $response")
            return flow {
                for (image in imageList) {
                    val buildImageURL = buildImageURL(
                        image,
                        previewToken
                    )
                    Log.v(TAG,"Imageurl is: $buildImageURL")
                    val future = Glide.with(appContext)
                        .asBitmap()
                        .load(
                            buildImageURL
                        )
                        .submit()
                    val img = ImageUiState(
                        future.get(),
                        image.title,
                        image.takenAt,
                        image.hash,
                        image.photoID,
                        image.photoUID
                    )
                    emit(img)
                }
            }


        }
        return null


    }

    private fun buildImageURL(
        image: PhotoResponseDAO,
        previewToken: String,
        thumbSize: String = "fit_2048"
    ): String {
        //TODO: Escape previewtoken to prevent XSS?
        return (usedCredentials?.getCheckedHostUrl()
            ?: "") + "api/v1/t/" + image.hash + "/" + previewToken + "/" + thumbSize
    }
}

data class ImageUiState(
    val bitmap: Bitmap,
    val title: String,
    val dateTaken: LocalDateTime,
    val hash: String,
    val photoID: String,
    val photoUID: String
)
data class APIErrorMessage(
    val error: String
)

data class SessionAnswer(
    val data: SessionAnwerDataDAO
)

data class SessionAnwerDataDAO(
    val user: PhotoPrismUserDAO
)

data class PhotoPrismUserDAO(
    @SerializedName("uid") val userID: String,
    @SerializedName("FullName") val fullName: String,
)

data class PhotoResponseDAO(
    @SerializedName("ID") val photoID: String,
    @SerializedName("UID") val photoUID: String,
    @SerializedName("Type") val photoType: String,
    @SerializedName("TakenAt") val takenAt: LocalDateTime,
    @SerializedName("Title") val title: String,
    @SerializedName("Hash") val hash: String

)


