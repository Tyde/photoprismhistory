package de.danielthiem.photoprismhistory.data

import android.util.Log
import androidx.work.*
import cafe.adriel.voyager.core.model.ScreenModel
import cafe.adriel.voyager.core.model.coroutineScope
import de.danielthiem.photoprismhistory.workers.FetchTodaysImagesWorker
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch
import java.time.Duration
import java.time.Instant
import java.time.LocalDateTime
import java.time.temporal.ChronoUnit
import java.time.temporal.TemporalUnit
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class LoginScreenModel @Inject constructor(
    val photoprismDataSource: PhotoprismDataSource,
    val credentialRepository: CredentialRepository,
    val workManager: WorkManager,
    val cachedImagesRepository: CachedImagesRepository
) : ScreenModel {
    private val _data = MutableStateFlow(
        PhotoPrismCredentials(
            "https://photo.danielthiem.eu",
            "admin",
            "4U8FRjsM\$nLzE!J3a"
        )
    )
    val data = _data.asStateFlow()
    val TAG = "LoginScreenModel"

    private val _isLoading = MutableStateFlow(false)
    val isLoading = _isLoading.asStateFlow()

    fun tryLoginFromStoredCredentials(onResult: (PhotoprismDataSource.LoginResultData) -> Unit) {
        val creds = credentialRepository.getCredentials()
        if (creds != null) {
            coroutineScope.launch {
                _isLoading.value = true
                if (photoprismDataSource.trySavedSession(creds.getCheckedHostUrl())) {
                    println("Got true")
                    val result = PhotoprismDataSource.LoginResultData(true)
                    onResult(result)
                    return@launch
                }


                updateHost(creds.hostUrl)
                updateUsername(creds.username)
                updatePassword(creds.password)
                _isLoading.value = false
                tryLogin(onResult, false)
            }
        } else {
            onResult(PhotoprismDataSource.LoginResultData(false, "no credentials"))
        }
    }

    fun updateHost(newHostName: String) {
        println("New hostname")
        _data.value = _data.value.copy(hostUrl = newHostName)
    }

    fun updateUsername(newUsername: String) {
        println("New username $newUsername")
        _data.value = _data.value.copy(username = newUsername)
    }

    fun updatePassword(newPassword: String) {
        _data.value = _data.value.copy(password = newPassword)
    }

    fun tryLogin(

        onResult: (PhotoprismDataSource.LoginResultData) -> Unit,
        storeCredentialsOnSuccess: Boolean = true
    ) {
        _isLoading.value = true
        coroutineScope.launch {
            val stableCredentials = data.value.copy()
            val result = photoprismDataSource.login(stableCredentials)
            _isLoading.value = false
            if (result.successful) {
                credentialRepository.saveCredentials(stableCredentials)

            }
            onResult(result)
        }
    }

    fun checkForCurrentCachedImages(onResult: (Boolean) -> Unit) {
        coroutineScope.launch(Dispatchers.IO) {
            val success =  cachedImagesRepository.hasImangeForToday()
            onResult(success)
        }
    }

    fun installWorkRequests(runAtHour: Int) {
        //First calculate the initial delay necessary as the WorkRequest API does not allow
        //a defined starting point for PeriodicWorks
        val startOfToday = LocalDateTime.now().truncatedTo(ChronoUnit.DAYS)
        val duration = when {
            LocalDateTime.now().hour < runAtHour -> {
                Duration.between(
                    LocalDateTime.now(),
                    startOfToday.plusHours(runAtHour.toLong())
                )
            }
            else -> {
                Duration.between(
                    LocalDateTime.now(),
                    startOfToday.plusDays(1).plusHours(runAtHour.toLong())
                )
            }
        }

        val constraints = Constraints.Builder()
            .setRequiredNetworkType(NetworkType.UNMETERED)
            .setRequiresBatteryNotLow(true)
            .build()
        //set to start at 9am but if this is set before 9am then start today, otherwise tomorrow!
        val workRequest = PeriodicWorkRequestBuilder<FetchTodaysImagesWorker>(
            repeatInterval = 1,
            repeatIntervalTimeUnit = TimeUnit.DAYS,
            flexTimeInterval = 4,
            flexTimeIntervalUnit = TimeUnit.HOURS
        )
            .setInitialDelay(duration)
            .setConstraints(constraints)
            .setBackoffCriteria(
                BackoffPolicy.LINEAR,
                10,
                TimeUnit.MINUTES
            )
            .build()
        workManager.enqueueUniquePeriodicWork(
            "dailyLoader",
            ExistingPeriodicWorkPolicy.REPLACE,
            workRequest
        )
    }
}