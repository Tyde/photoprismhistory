package de.danielthiem.photoprismhistory.data

import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import androidx.work.OneTimeWorkRequestBuilder
import androidx.work.WorkManager
import cafe.adriel.voyager.core.model.ScreenModel
import cafe.adriel.voyager.core.model.coroutineScope
import de.danielthiem.photoprismhistory.workers.FetchTodaysImagesWorker
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject

class ImagesScreenModel @Inject constructor(
    val photoprismDataSource: PhotoprismDataSource,
    val workManager: WorkManager,
    val cachedImagesRepository: CachedImagesRepository
) : ScreenModel {
    private val _images = MutableStateFlow(listOf<ImageUiState>())
    val images = _images.asStateFlow()
    private var _currentIndex = MutableStateFlow(-1)
    val currentIndex = _currentIndex.asStateFlow()
    private val _currentImage = MutableStateFlow<ImageUiState?>(null)
    val currentImage = _currentImage.asStateFlow()


    fun requestTodaysImages() {
        println("Started request")
        coroutineScope.launch(Dispatchers.IO) {
            photoprismDataSource.getHistoryOfToday()?.collect { nextDrawable ->
                val mutList = _images.value.toMutableList()
                mutList.add(nextDrawable)
                _images.value = mutList
                if (_images.value.size == 1) {
                    _currentImage.value = images.value[0]
                    _currentIndex.value = 0
                }
            }
        }
    }

    fun loadImagesFromCache() {
        coroutineScope.launch(Dispatchers.IO) {
            _images.value =cachedImagesRepository.getImagesForToday()
        }
    }

    fun nextImage() {
        if (currentIndex.value <= images.value.size - 2) {
            _currentIndex.value = currentIndex.value + 1
        } else {
            _currentIndex.value = 0
        }
        _currentImage.value = images.value[_currentIndex.value]
    }

    fun previousImage() {
        if (currentIndex.value > 0) {
            _currentIndex.value = currentIndex.value - 1
        } else {
            _currentIndex.value = images.value.size - 1
        }
        _currentImage.value = images.value[_currentIndex.value]
    }

    fun testWorker() {
        val workRequest = OneTimeWorkRequestBuilder<FetchTodaysImagesWorker>()
            .build()
        workManager.enqueue(workRequest)

    }


}