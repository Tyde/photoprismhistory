package de.danielthiem.photoprismhistory.di

import cafe.adriel.voyager.core.model.ScreenModel
import cafe.adriel.voyager.hilt.ScreenModelKey
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityComponent
import dagger.multibindings.IntoMap
import de.danielthiem.photoprismhistory.data.ImagesScreenModel
import de.danielthiem.photoprismhistory.data.LoginScreenModel

@Module
@InstallIn(ActivityComponent::class)
abstract class ImagesModule {

    @Binds
    @IntoMap
    @ScreenModelKey(ImagesScreenModel::class)
    abstract fun bindImagesScreenModel(imagesScreenModel: ImagesScreenModel) : ScreenModel
}