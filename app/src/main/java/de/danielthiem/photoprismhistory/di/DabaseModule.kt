package de.danielthiem.photoprismhistory.di

import android.content.Context
import androidx.room.Room
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import de.danielthiem.photoprismhistory.db.AppDatabase
import de.danielthiem.photoprismhistory.db.CachedImageDao
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
object DabaseModule {

    @Provides
    fun provideCachedImageDap(database: AppDatabase): CachedImageDao {
        return database.cachedImageDao()
    }

    @Provides
    @Singleton
    fun provideDatabase(@ApplicationContext appContext:Context): AppDatabase {
        return Room.databaseBuilder(
            appContext,
            AppDatabase::class.java,
            "cachedImages.db"
        ).build()
    }
}