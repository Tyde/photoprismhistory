package de.danielthiem.photoprismhistory.di

import android.content.Context
import android.util.Log
import cafe.adriel.voyager.core.model.ScreenModel
import cafe.adriel.voyager.hilt.ScreenModelKey
import com.google.gson.*
import com.google.gson.JsonSerializer
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityComponent
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import dagger.multibindings.IntoMap
import de.danielthiem.photoprismhistory.PhotoprismHistoryApplication
import de.danielthiem.photoprismhistory.data.CredentialRepository
import de.danielthiem.photoprismhistory.data.CredentialRepositoryKeystoreImpl
import de.danielthiem.photoprismhistory.data.LoginScreenModel
import io.ktor.client.*
import io.ktor.client.engine.cio.*
import io.ktor.client.engine.okhttp.*
import io.ktor.client.features.*
import io.ktor.client.features.json.*
import io.ktor.client.features.logging.*
import javax.inject.Singleton

import java.lang.reflect.Type
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter


@Module
@InstallIn(ActivityComponent::class)
abstract class LoginModule {
    @Binds
    @IntoMap
    @ScreenModelKey(LoginScreenModel::class)
    abstract fun bindLoginScreenModel(loginScreenModel: LoginScreenModel): ScreenModel


}

@Module
@InstallIn(SingletonComponent::class)
abstract class CredentialModule {
    @Binds
    abstract fun bindCredentialRepository(impl: CredentialRepositoryKeystoreImpl): CredentialRepository

}

@InstallIn(SingletonComponent::class)
@Module
object InternetInteractionModule {
    @Provides
    fun provideKtorClient(): HttpClient {
        return HttpClient(OkHttp) {
            install(JsonFeature) {

                serializer = GsonSerializer {
                    registerTypeAdapter(LocalDateTime::class.java, LocalDateTimeConverter())
                }
            }
            install(Logging) {
                logger = CustomHttpLogger()
                expectSuccess = false
                level = LogLevel.ALL
            }
            install(HttpTimeout)
        }
    }


}

class LocalDateTimeConverter : JsonSerializer<LocalDateTime>, JsonDeserializer<LocalDateTime> {

    override fun serialize(
        src: LocalDateTime?,
        typeOfSrc: Type?,
        context: JsonSerializationContext?
    ): JsonElement? {
        return JsonPrimitive(FORMATTER.format(src))
    }

    override fun deserialize(
        json: JsonElement?,
        typeOfT: Type?,
        context: JsonDeserializationContext?
    ): LocalDateTime? {
        return LocalDateTime.from(FORMATTER.parse(json!!.asString))
    }

    companion object {
        private val FORMATTER = DateTimeFormatter.ISO_OFFSET_DATE_TIME
    }
}

class CustomHttpLogger() : Logger {
    override fun log(message: String) {
        Log.d("loggerTag", message) // Or whatever logging system you want here
    }
}