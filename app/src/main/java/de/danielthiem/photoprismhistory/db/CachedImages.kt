package de.danielthiem.photoprismhistory.db

import androidx.room.*
import java.time.LocalDate
import java.time.LocalDateTime
import javax.inject.Inject

@Entity(tableName = "cached_image")
data class CachedImage(
    @PrimaryKey(autoGenerate = true) val dbID: Int,
    val hash: String,
    @ColumnInfo(name = "time_downloaded") val timeDownloaded: LocalDateTime,
    @ColumnInfo(name = "photoprism_id") val photoPrismId: String,
    @ColumnInfo(name = "photoprism_uid") val photoPrismUid: String,
    @ColumnInfo(name = "taken_at") val takenAt: LocalDateTime,
    val title: String,
    @ColumnInfo(name = "file_name") var fileName: String?
)


@Dao
interface CachedImageDao {

    @Query("SELECT * FROM cached_image")
    fun getAll(): List<CachedImage>

    @Query("SELECT * FROM cached_image WHERE time_downloaded > :beginTs AND time_downloaded < :endTs")
    fun getAllBetweenTimestamps(beginTs: Long, endTs: Long): List<CachedImage>

    @Update()
    fun updateCachedImage(image: CachedImage)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertCachedImage(cachedImage: CachedImage): Long

    @Query("SELECT * FROM cached_image WHERE dbID == :id")
    fun getById(id: Long): CachedImage
}

