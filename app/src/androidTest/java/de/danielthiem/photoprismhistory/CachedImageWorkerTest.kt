package de.danielthiem.photoprismhistory

import android.content.Context
import android.util.Log
import androidx.hilt.work.HiltWorkerFactory
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.work.*
import androidx.work.testing.SynchronousExecutor
import androidx.work.testing.TestListenableWorkerBuilder
import androidx.work.testing.TestWorkerBuilder
import androidx.work.testing.WorkManagerTestInitHelper
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.android.testing.HiltAndroidRule
import dagger.hilt.android.testing.HiltAndroidTest
import de.danielthiem.photoprismhistory.workers.FetchTodaysImagesWorker
import de.danielthiem.photoprismhistory.workers.FetchTodaysImagesWorker_Factory
import de.danielthiem.photoprismhistory.workers.FetchTodaysImagesWorker_HiltModule
import kotlinx.coroutines.runBlocking

import org.junit.Test
import org.junit.runner.RunWith

import org.junit.Assert.*
import org.junit.Before
import org.junit.Rule
import java.util.concurrent.Executor
import java.util.concurrent.Executors
import javax.inject.Inject

/**
 * Instrumented test, which will execute on an Android device.
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
@RunWith(AndroidJUnit4::class)
@HiltAndroidTest
class CachedImageWorkerTest {

    @get:Rule
    var hiltRule = HiltAndroidRule(this)

    private lateinit var executor:Executor

    lateinit var workManager: WorkManager
    @Before
    fun init() {

        val config = Configuration.Builder()
            .setMinimumLoggingLevel(Log.DEBUG)
            .setExecutor(SynchronousExecutor())
            .build()
        hiltRule.inject()
        WorkManagerTestInitHelper.initializeTestWorkManager(context,config)
        workManager = WorkManager.getInstance(context)
    }


    @Inject
    @ApplicationContext
    lateinit var context: Context




    @Test
    fun testWorker() = runBlocking {
        println(context)
        val request = OneTimeWorkRequestBuilder<FetchTodaysImagesWorker>()
            .build()
        workManager.enqueue(request).result.get()

        val result = workManager.getWorkInfoById(request.id).get()
        assertEquals(result,ListenableWorker.Result.Success())
    }
}